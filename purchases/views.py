import datetime
import time
from collections import OrderedDict

import csv

from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.conf import settings
from django.db.models import F, ExpressionWrapper, FloatField
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import render_to_string

from purchases.models import Purchase


DATE_POST_FORMAT = '%Y-%m-%d'
IDS_ON_PAGE = 10


def index(request):
    return redirect('/main')


def main(request, page=1):
    global IDS_ON_PAGE
    if 'infinite' in request.path:
        IDS_ON_PAGE = 25

    dstart = request.POST.get('date_start') or request.GET.get('date_start')
    dstop = request.POST.get('date_stop') or request.GET.get('date_stop')
    get_csv = request.GET.get('csv')

    page = request.GET.get('page') or int(page)
    p_ids = Purchase.objects.order_by('pm_id').distinct().values_list('pm_id', flat=True)

    if not get_csv:
        paginator = Paginator(p_ids, IDS_ON_PAGE)
        try:
            p_ids = paginator.page(page)
        except PageNotAnInteger:
            p_ids = paginator.page(1)
        except EmptyPage:
            p_ids = paginator.page(paginator.num_pages)

    purchases_from_db = Purchase.objects.filter(pm_id__in=p_ids)

    if dstart and not dstart == 'None':
        try:
            t = time.strptime(dstart, DATE_POST_FORMAT)
            dstart = datetime.date(*t[:3])
        except ValueError:
            raise Http404()
        purchases_from_db = purchases_from_db.filter(created__gte=dstart)
        dstart = dstart.strftime(DATE_POST_FORMAT)
    if dstop and not dstop == 'None':
        try:
            t = time.strptime(dstop, DATE_POST_FORMAT)
            dstop = datetime.date(*t[:3])
        except ValueError:
            raise Http404()
        purchases_from_db = purchases_from_db.filter(created__lte=dstop)
        dstop = dstop.strftime(DATE_POST_FORMAT)

    purchases_from_db = purchases_from_db.annotate(total_sum=ExpressionWrapper(F('a_count') * F('price'), output_field=FloatField()))
    purchases_from_db = purchases_from_db.order_by('pm_id', 'price')

    dates = set(purchases_from_db.values_list('created', flat=True))
    dates = sorted(list(dates))

    purchases = OrderedDict()

    for purchase in purchases_from_db:
        purchases.setdefault(purchase.pm_id, OrderedDict())
        purchases[purchase.pm_id].setdefault(purchase.price, OrderedDict((date, 0) for date in dates))
        purchases[purchase.pm_id][purchase.price][purchase.created] = purchase.total_sum

    if get_csv:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)

        headers = ['pm_id', 'price']
        headers.extend(dates)

        writer.writerow(headers)
        for pm_id, data in purchases.items():
            for price, dates in data.items():
                row = [pm_id, price]
                for date, amount in dates.items():
                    row.append(amount)
                writer.writerow(row)

        return response

    if request.is_ajax():
        data = {
            'table': render_to_string('table_body.html', {'purchases': purchases}),
            'has_next': p_ids.has_next(),
            'has_previous': p_ids.has_previous()
        }
        return JsonResponse(data)

    context = {
        'date_start': dstart or '',
        'date_stop': dstop or '',
        'dates': dates,
        'purchases': purchases,
        'pagination': p_ids
    }
    return render(request, 'index.html', context)


def desc(request):
    fname = settings.BASE_DIR + '/task_description/task.txt'
    context = {
        'text': ''
    }
    with open(fname) as f:
        context['text'] += ''.join(f.readlines())

    return render(request, 'desc.html', context)
