from __future__ import unicode_literals

from django.db import models


class Purchase(models.Model):
    created = models.DateField(blank=False, null=False)
    pm_id = models.IntegerField(null=True, blank=True)
    price = models.FloatField(blank=False, null=False)
    a_count = models.IntegerField(blank=False, null=False)
