import datetime
import random
from optparse import make_option
from tqdm import tqdm

from django.core.management import BaseCommand, CommandError
from purchases.models import Purchase

DATE_FORMAT = '%d.%m.%Y'
ROWS_NUMBER = 10000
ITEMS_COUNT = 500

MIN_PRICE = 100
MAX_PRICE = 99999

MAX_ITEM_PRICES = 2


def generate_ids(n, start=1):
    if n <= 0:
        n = ITEMS_COUNT
    result = set()
    while len(result) < n:
        result.add(random.randint(start, start+n))
    return list(result)


def generate_date(dstart, days):
    return dstart + datetime.timedelta(days=random.randint(0, days))


def main(rows_number, dstart, dstop, items_count):
    """

    Args:
        items_count (int):
        rows_number (int):
        dstop (datetime.date):
        dstart (datetime.date):
    """
    ids = {k: [] for k in generate_ids(items_count)}
    days = (dstop - dstart).days

    purchases = []

    for _ in tqdm(range(0, rows_number)):
        pm_id = random.choice(ids.keys())
        if len(ids[pm_id]) <= MAX_ITEM_PRICES:
            price = random.uniform(MIN_PRICE, MAX_PRICE)
            price = round(price, 2)
            ids[pm_id].append(price)
        else:
            price = random.choice(ids[pm_id])

        a_count = random.randint(0, 10)
        date = generate_date(dstart, days)

        purchases.append(
            Purchase(
                pm_id=pm_id,
                a_count=a_count,
                created=date,
                price=price
            )
        )

    Purchase.objects.bulk_create(purchases)


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('-r', '--rows',
                    action='store',
                    dest='rows_number',
                    type='int',
                    help='Number of rows to generate',
                    default=ROWS_NUMBER),
        make_option('-i', '--items',
                    action='store',
                    dest='items_count',
                    type='int',
                    help='Number of items',
                    default=ITEMS_COUNT),
    )

    def handle(self, *arg, **opt):
        try:
            rows_number = int(opt['rows_number'])
        except ValueError:
            raise CommandError('number of rows should be int')

        today = datetime.date.today()
        dstart = today.replace(day=1)
        dstop = dstart + datetime.timedelta(days=32)
        dstop = dstop.replace(day=1) - datetime.timedelta(days=1)

        main(rows_number, dstart, dstop, opt['items_count'])
