from django import template

register = template.Library()


@register.filter
def dict_lookup(dct, key):
    if not isinstance(dct, dict):
        return u''
    return dct.get(key, '')
